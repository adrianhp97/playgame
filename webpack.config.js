const path = require('path');
const config = {
  BUILD_DIR: path.resolve(__dirname, './dist'),
  APP_DIR: path.resolve(__dirname, './src'),
  PUBLIC_DIR: path.resolve(__dirname, './public'),
  ROOT_DIR: __dirname
}

module.exports = function buildConfig(env) {
  config.ENV = env;
  if (env === 'dev' || env === 'prod') {
    return require('./config/' + env + '.js')(config);
  } else {
    console.log("Wrong webpack build parameter. Possible choices: 'dev' or 'prod'.")
  }
};
