import React, { useState } from 'react'
import { Redirect, RouteComponentProps } from 'react-router'

/** Styles */
import s from './s.module.scss'

/** Hooks */
import { useAuthState, useAuthDispatch } from 'src/store/modules/auth/hook'

/** Images */
import logo from 'src/assets/img/logo.png'

const LandingPage: React.FC<RouteComponentProps> = () => {
  const { username } = useAuthState()
  const { setUsername } = useAuthDispatch()

  const [usernameField, setUsernameField] = useState<string>(username || '')

  const submit = (
    event: React.KeyboardEvent<HTMLInputElement>
  ): JSX.Element | void => {
    if (event.key === 'Enter') {
      setUsername((event.target as HTMLInputElement).value)
      return <Redirect to="/select-level" />
    }
  }

  return (
    <div className={s.container}>
      <div>
        <input
          placeholder="Username"
          value={usernameField}
          onKeyDown={submit}
          onChange={(event) => setUsernameField(event.target.value)}
        />
      </div>
      <div className={s.logo}>
        <img src={logo} />
      </div>
    </div>
  )
}

export default LandingPage
