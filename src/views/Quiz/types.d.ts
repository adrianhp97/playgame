export interface Level {
  Question: string
  Answer: string
}

export interface Params {
  id: string
}
