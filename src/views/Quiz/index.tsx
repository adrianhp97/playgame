import React, { useState } from 'react'
import { Redirect, RouteComponentProps, useParams } from 'react-router'

/** Hooks */
import { useHistory } from 'react-router-dom'
import { useAuthState, useAuthDispatch } from 'src/store/modules/auth/hook'

/** Styles */
import s from './s.module.scss'

/** Data */
import data from 'src/assets/data/puzzle'

/** Images */
import btnBlue from 'src/assets/img/button_blue.png'
import happy from 'src/assets/img/tini_happy.png'

/** Types */
import { Level, Params } from './types'

const idQuestionList: string[] = data.map((item: Level): string => item.Question)

const Quiz: React.FC<RouteComponentProps> = () => {
  const { id }: Params = useParams()
  const history = useHistory()

  if (!id || !idQuestionList.includes(id)) {
    return <Redirect to="/select-level" />
  }

  const { point } = useAuthState()
  const { setPoint } = useAuthDispatch()

  const [answer, setAnswer] = useState<string>('')
  const [isError, setIsError] = useState<boolean>(false)
  const [isTrue, setIsTrue] = useState<boolean>(false)

  const changeAnswer = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setIsError(false)
    setAnswer(event.target.value)
  }

  const submit = (): void => {
    const idx = idQuestionList.indexOf(id)

    if (idx >= 0) {
      if (data[idx].Answer.toLowerCase() === answer.toLowerCase()) {
        setIsTrue(true)
        setIsError(false)
        setAnswer('')
        if (point) {
          setPoint(+point + 10)
        } else {
          setPoint(10)
        }
      } else {
        setIsError(true)
      }
    }
  }

  const next = (): void => {
    const possibleQuiz = idQuestionList.filter(
      (item: string | number): boolean => item !== id
    )

    const idx = Math.floor(Math.random() * possibleQuiz.length)
    setIsTrue(false)
    history.push(`/quiz/${possibleQuiz[idx]}`)
  }

  return (
    <div className={s.container}>
      <div className={s.levelWrapper}>
        <img src={!isTrue ? `/public/img/${id}.jpg` : happy} />
      </div>
      {!isTrue && (
        <>
          <div>
            <input value={answer} onChange={changeAnswer} onKeyDown={submit} />
          </div>
          {isError && (
            <div className={s.errorContainer}>
              <span>Oops!</span> Jawaban anda salah..
            </div>
          )}
        </>
      )}
      {isTrue && (
        <div className={s.trueContainer}>
          <span>Selamat!</span> Jawaban anda benar!
        </div>
      )}
      <div className={s.btnSubmit} onClick={isTrue ? next : submit}>
        <img src={btnBlue} />
        <span>{isTrue ? 'NEXT' : 'SUBMIT'}</span>
      </div>
    </div>
  )
}

export default Quiz
