import React from 'react'
import { RouteComponentProps } from 'react-router'

/** Hooks */
import { useHistory } from 'react-router-dom'

/** Styles */
import s from './s.module.scss'

/** Data */
import data from 'src/assets/data/puzzle'

/** Images */
import logo from 'src/assets/img/logo.png'

/** Types */
import { Level } from './types'

const SelectLevel: React.FC<RouteComponentProps> = () => {
  const history = useHistory()

  const goToLevel = (value: string | number) => {
    history.push(`/quiz/${value}`)
  }

  return (
    <div className={s.container}>
      <div className={s.levelWrapper}>
        <div className={s.levelContainer}>
          {data.map((item: Level) => (
            <img
              onClick={() => goToLevel(item.Question)}
              src={`/public/img/${item.Question}.jpg`}
              key={item.Question}
            />
          ))}
        </div>
      </div>
      <div className={s.logo}>
        <img src={logo} />
      </div>
    </div>
  )
}

export default SelectLevel
