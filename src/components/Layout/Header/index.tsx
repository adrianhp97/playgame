import React from 'react'

/** Components */
import Point from 'src/components/Point'

/** Hooks */
import { useAuthState } from 'src/store/modules/auth/hook'

/** Types */
import { Props } from './types'

/** Styles */
import s from './s.module.scss'

const Header: React.FC<Props> = ({ title, showPoint }: Props) => {
  const { point } = useAuthState()

  return (
    <div className={s.header}>
      {!showPoint ? (
        <div className={s.title}>{title}</div>
      ) : (
        <Point value={point || 0} />
      )}
    </div>
  )
}

export default Header
