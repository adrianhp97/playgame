import { RouteComponentProps } from 'react-router'
import { routeMeta } from 'src/routes/types'

export interface Props {
  children: (matchProps: RouteComponentProps) => JSX.Element | null
  matchProps: RouteComponentProps
  meta?: routeMeta
}
