import React from 'react'

/** Components */
import Header from 'src/components/Layout/Header'

/** Types */
import { Props } from './types'

/** Styles */
import s from './s.module.scss'

const BaseLayout: React.FC<Props> = ({ children, matchProps, meta }: Props) => {
  return (
    <div className={s.container}>
      <Header title={meta?.title || ''} showPoint={meta?.showPoint} />
      <div className={s.content}>{children(matchProps)}</div>
    </div>
  )
}

export default BaseLayout
