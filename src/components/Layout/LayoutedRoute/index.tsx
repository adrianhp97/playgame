import React from 'react'
import { Route, RouteComponentProps, Redirect } from 'react-router'

/** Components */
import Base from 'src/components/Layout/Base'

/** Hooks */
import { useAuthState } from 'src/store/modules/auth/hook'

/** Types */
import { routeType } from 'src/routes/types'

const LayoutedRoute: React.FC<routeType> = ({ component, ...rest }: routeType) => {
  const { username } = useAuthState()

  if (!username) {
    if (rest.meta && rest.meta.auth) {
      return <Redirect to="/" />
    }
  } else {
    if (rest.meta && rest.meta.name === 'Landing Page') {
      return <Redirect to="/select-level" />
    }
  }

  return (
    <Route
      {...rest}
      render={(matchProps) => (
        <>
          <Base matchProps={matchProps} meta={rest.meta}>
            {(routerProps: RouteComponentProps) => component(routerProps)}
          </Base>
        </>
      )}
    />
  )
}

export default LayoutedRoute
