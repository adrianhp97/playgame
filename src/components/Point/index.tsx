import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'

/** Types */
import { Props } from './types'

/** Styles */
import s from './s.module.scss'

const Point: React.FC<Props> = ({ value }: Props) => {
  return (
    <div className={s.wrapper}>
      <span className={s.star}>
        <FontAwesomeIcon icon={faStar} />
      </span>
      <div className={s.value}>{value}</div>
    </div>
  )
}

export default Point
