import { RouteComponentProps } from 'react-router'

/** Types */
import { routeType } from './types'

/** Views */
import LandingPage from 'src/views/LandingPage'
import SelectLevel from 'src/views/SelectLevel'
import Quiz from 'src/views/Quiz'

const paths: routeType[] = [
  {
    path: '/',
    exact: true,
    component: (matchProps: RouteComponentProps): JSX.Element | null =>
      LandingPage(matchProps),
    meta: {
      name: 'Landing Page',
      title: 'Masukkan Nama Anda',
      auth: false,
    },
  },
  {
    path: '/select-level',
    exact: true,
    component: (matchProps: RouteComponentProps): JSX.Element | null =>
      SelectLevel(matchProps),
    meta: {
      name: 'Select Level',
      title: 'Pilih Level',
      auth: true,
    },
  },
  {
    path: '/quiz/:id',
    exact: true,
    component: (matchProps: RouteComponentProps): JSX.Element | null =>
      Quiz(matchProps),
    meta: {
      name: 'Quiz',
      title: 'Quiz',
      showPoint: true,
      auth: true,
    },
  },
]

export default paths
