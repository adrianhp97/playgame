import { RouteComponentProps } from 'react-router'

export type routeMeta = {
  name?: string
  title?: string
  auth?: boolean
  showPoint?: boolean
}

export type routeType = {
  path: string
  exact?: boolean
  component: ({
    match,
    location,
    history,
  }: RouteComponentProps) => JSX.Element | null
  meta?: routeMeta
}
