import React from 'react'
import { Router, Switch, Redirect, Route } from 'react-router'

import paths from './paths'

import history from 'src/utils/history'

import LayoutedRoute from 'src/components/Layout/LayoutedRoute'

const Routes: React.FC = () => {
  return (
    <>
      <Router history={history}>
        <Switch>
          {paths.map((route, idx) => (
            <LayoutedRoute key={idx} {...route} />
          ))}
          <Route render={() => <Redirect to="/" />} />
        </Switch>
      </Router>
    </>
  )
}

export default Routes
