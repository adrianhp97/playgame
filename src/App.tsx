import React from 'react'
import Routes from './routes'
import ContextState from './store/ContextState'

const App: React.FunctionComponent = () => {
  return <ContextState>{() => <Routes />}</ContextState>
}

export default App
