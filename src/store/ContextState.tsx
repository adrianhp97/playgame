import React from 'react'

import AuthContext from 'store/modules/auth/Context'

interface Props {
  children: () => JSX.Element | null
}

const ContextState: React.FC<Props> = ({ children }: Props) => {
  return <AuthContext>{children}</AuthContext>
}

export default ContextState
