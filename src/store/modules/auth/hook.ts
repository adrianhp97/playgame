import React from 'react'

import { AuthDispatchContext, AuthStateContext } from './Context'
import { IState } from './types'

export const useAuthState = (): Partial<IState> => {
  const context = React.useContext(AuthStateContext)
  if (context === undefined) {
    throw new Error('Need to wrapped in Context Provider')
  }
  return context
}

export const useAuthDispatch = (): any => {
  const context = React.useContext(AuthDispatchContext)
  if (context === undefined) {
    throw new Error('Need to wrapped in Context Provider')
  }
  return context
}
