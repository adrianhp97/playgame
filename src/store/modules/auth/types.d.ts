export const enum ActionType {
  SET_USERNAME = 'SET_USERNAME',
  SET_POINT = 'SET_POINT',
}

export interface IAction {
  type: ActionType
  payload: {
    username?: string | null
    point?: string | null
  }
}

export interface IState {
  username?: string | null
  point?: string | null
}
