import React from 'react'
import { IAction, ActionType, IState } from './types'

export const initialState: IState = {
  username: sessionStorage.getItem('username'),
  point: sessionStorage.getItem('point'),
}

export const authReducers: React.Reducer<IState, IAction> = (
  state: IState,
  action: IAction
) => {
  switch (action.type) {
    case ActionType.SET_USERNAME:
      sessionStorage.setItem('username', action.payload.username || '')
      return {
        ...state,
        username: action.payload.username,
      }
    case ActionType.SET_POINT:
      sessionStorage.setItem('point', action.payload.point || '')
      return {
        ...state,
        point: action.payload.point,
      }
    default:
      throw new Error()
  }
}
