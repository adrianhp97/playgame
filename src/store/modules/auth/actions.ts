import { ActionType, IAction } from './types'

export const setUsername = (value: string | null): IAction => {
  return {
    type: ActionType.SET_USERNAME,
    payload: {
      username: value,
    },
  }
}

export const setPoint = (value: string | null): IAction => {
  return {
    type: ActionType.SET_POINT,
    payload: {
      point: value,
    },
  }
}
