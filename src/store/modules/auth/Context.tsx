import React, { useReducer } from 'react'

/** Reducers */
import { authReducers, initialState } from './reducers'

/** Actions */
import * as actions from './actions'

/** Types */
import { IState } from './types'

interface Props {
  children: () => JSX.Element | null
}

export const AuthStateContext = React.createContext<Partial<IState>>(initialState)
export const AuthDispatchContext = React.createContext<any>(null)

const AuthContext: React.FC<Props> = ({ children }: Props) => {
  const [state, dispatch] = useReducer(authReducers, initialState)

  const setUsername = (value: string | null) => dispatch(actions.setUsername(value))
  const setPoint = (value: string | null) => dispatch(actions.setPoint(value))

  return (
    <AuthDispatchContext.Provider
      value={{
        dispatch,
        setUsername,
        setPoint,
      }}
    >
      <AuthStateContext.Provider value={state}>
        {children()}
      </AuthStateContext.Provider>
    </AuthDispatchContext.Provider>
  )
}

export default AuthContext
