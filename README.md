## PlayGame Heroku

#### Introduction:

PlayGame

Implement single page application (SPA) for playing quiz games

### URL

[playgame-ts](https://playgame-ts.herokuapp.com)

## Installation and Setup Instructions

#### Development:  

Clone down this repository. You will need `node` and `npm` installed globally on your machine.

Installation:

`npm install`

To Start Server:

`npm run dev`

To Visit App:

[localhost:8080](http://localhost:8080/)  


#### Production:  

Clone down this repository. You will need `node` and `npm` installed globally on your machine.

Installation:

`npm run build`

To Start Server:

`npm start`  

To Visit App:

[localhost:8080](http://localhost:8080/)  