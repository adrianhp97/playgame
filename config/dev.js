const webpack = require('webpack');

module.exports = function (config) {
  const devConfig = Object.assign({}, require('./common')(config));

  devConfig.devtool = 'source-map';
  devConfig.mode = 'development';
  devConfig.devServer = {
    historyApiFallback: true
  };

  return devConfig;
}
