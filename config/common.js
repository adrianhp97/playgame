const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = function (config) {
  return {
    entry: config.APP_DIR + '/index.tsx',
    output: {
      path: config.BUILD_DIR,
      filename: config.ENV + '-bundle.js',
      publicPath: '/'
    },
    resolve: {
      // modules: [config.ROOT_DIR + '/node_modules'],
      extensions: ['.ts', '.tsx', '.js', '.css', '.scss'],
      alias: {
        'src': config.APP_DIR + '/',
        'components': config.APP_DIR + '/components',
        'views': config.APP_DIR + '/views',
        'store': config.APP_DIR + '/store',
        'assets': config.APP_DIR + '/assets',
        'utils': config.APP_DIR + '/utils'
      }
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: config.PUBLIC_DIR + '/index.html'
      }),
      new CopyWebpackPlugin({
        patterns: [
          { from: 'public/img', to: 'public/img' }
        ]
      })
    ],
    optimization: {
      usedExports: true,
      splitChunks: {
        cacheGroups: {
          commons: {
            test: /[\\/]node_modules[\\/]/i,
            name: "vendors",
            chunks: "all"
          }
        }
      }
    },
    module: {
      rules: [
        {
          test: /\.ts(x?)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "ts-loader"
            }
          ],
          sideEffects: false
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          exclude: /node_modules/,
          use: [
            'file-loader'
          ],
          sideEffects: false
        },
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.s[ac]ss$/i,
          exclude: /\.module.(s(a|c)ss)$/,
          use: [
            'style-loader',
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: [
                  require('autoprefixer')
                ],
                modules: true
              }
            },
            'sass-loader',
          ],
          sideEffects: true
        },
        {
          test: /\.module.s[ac]ss$/i,
          use: [
            'style-loader',
            '@teamsupercell/typings-for-css-modules-loader',
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                modules: true,
              }
            },
            'sass-loader'
          ]
        },
      ]
    }
  }
}
