const webpack = require('webpack');
const common = require('./common');

module.exports = function(config) {
  const prodConfig = Object.assign({}, require('./common')(config));

  prodConfig.mode = 'production';

  return prodConfig;
}
